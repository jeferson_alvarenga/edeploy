//
//  SearchResultViewModel.swift
//  EDeployChallenge
//
//  Created by Jeferson Alvarenga on 06/05/18.
//  Copyright © 2018 DeepLogical. All rights reserved.
//

import UIKit

protocol SearchResultViewModelDelegate {
    func onPlaceLoaded(value: NSDecimalNumber)
    func startAnimating()
}

class SearchResultViewModel{
    var delegate: SearchResultViewModelDelegate!
    var selectedPlace: Place!
    
    init(delegate: SearchResultViewModelDelegate) {
        self.delegate = delegate
    }
    
    func loadPlace() {
        delegate.startAnimating()
        let networking = Networking()
        networking.request(NetworkConstant.placeAPI, parameters:["Nome": selectedPlace.city!, "Estado": selectedPlace.state!], method: "POST", onSuccess: onPlaceLoaded, onFailure: onPlaceLoadError) // TODO Localize string
    }
    
    func onPlaceLoaded(value: Any) {
        delegate.onPlaceLoaded(value: value as! NSDecimalNumber)
    }
    
    func onPlaceLoadError(error: String) {
        // TODO SHOW/LOG ERROR
    }
}
