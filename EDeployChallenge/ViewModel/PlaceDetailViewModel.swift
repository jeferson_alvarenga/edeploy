//
//  PlaceDetailViewModel.swift
//  EDeployChallenge
//
//  Created by Jeferson Alvarenga on 06/05/18.
//  Copyright © 2018 DeepLogical. All rights reserved.
//

import UIKit

protocol PlaceDetailViewModelDelegate {
    var place: Place? {get set}
    var score: NSDecimalNumber? { get set}
    func setScore()
}

class PlaceDetailViewModel {
    var place: Place?
    var delegate: PlaceDetailViewModelDelegate
    
    init(delegate: PlaceDetailViewModelDelegate) {
        self.delegate = delegate
    }
    
    func validate() {
        if let _ = delegate.score, let place = delegate.place, let _ = place.city {
            delegate.setScore()
        }else{
            // TODO SHOW/LOG ERROR
        }
    }
}
