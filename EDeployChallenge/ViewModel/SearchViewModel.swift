//
//  SearchViewModel.swift
//  EDeployChallenge
//
//  Created by Jeferson Alvarenga on 06/05/18.
//  Copyright © 2018 DeepLogical. All rights reserved.
//

protocol SearchViewModelDelegate {
    func onPlacesLoaded()
    func onPlacesLoadedError(error: String)
    func startAnimating()
}

class SearchViewModel {
    
    var delegate: SearchViewModelDelegate
    var places: Array<Place>?
    var city: String!
    var state: String!
    
    init(delegate: SearchViewModelDelegate) {
        self.delegate = delegate
    }
    
    func loadPlaces(city: String, state: String) {
        let networking = Networking()
        if networking.checkInternetConnection() {
            self.city = city
            self.state = state
            delegate.startAnimating()
            networking.requestMappable(NetworkConstant.placesAPI, mappable: Place.self, onSuccessMappable: onPlacesLoaded, onFailure: onPlacesLoadError)
        }else{
            onPlacesLoadError(error: NetworkError.NoInternetConnection.rawValue)
        }
    }
    
    func onPlacesLoaded(places: [Place]) {
        self.places = places.filter{($0.city?.lowercased().contains(city.lowercased()))! && ($0.state?.lowercased().contains(state.lowercased()))! }
        delegate.onPlacesLoaded()
    }
    
    func onPlacesLoadError(error: String) {
        delegate.onPlacesLoadedError(error: error)
    }
}
