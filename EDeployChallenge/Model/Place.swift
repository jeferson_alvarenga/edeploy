//
//  Place.swift
//  EDeployChallenge
//
//  Created by Jeferson Alvarenga on 06/05/18.
//  Copyright © 2018 DeepLogical. All rights reserved.
//

import Foundation
import ObjectMapper

//MARK: - Equatable Protocol implementation

func ==(lhs: Place, rhs: Place) -> Bool {
    return (lhs.city == rhs.city
        && lhs.state == rhs.state)
}

class Place: ModelBase {
    var city: String?
    var state: String?
    
    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    override func mapping(map: Map) {
        city <- map["Nome"]
        state <- map["Estado"]
    }
}
