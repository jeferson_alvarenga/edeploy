//
//  PlaceDetailViewController.swift
//  EDeployChallenge
//
//  Created by Jeferson Alvarenga on 06/05/18.
//  Copyright © 2018 DeepLogical. All rights reserved.
//

import UIKit

class PlaceDetailViewController: UIViewController, PlaceDetailViewModelDelegate {
    
    var place: Place?
    var score: NSDecimalNumber?
    var viewModel: PlaceDetailViewModel!

    @IBOutlet weak var txtScore: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        title = "Resultado" // TODO Localize string
        viewModel = PlaceDetailViewModel(delegate: self)
        viewModel.validate()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setScore() {
        guard let score = self.score, let place = self.place, let city = place.city else {
            return
        }
        txtScore.text = "A pontuação da cidade \(city) é de \(score)" // TODO Localize string
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
