//
//  PlaceTableViewCell.swift
//  EDeployChallenge
//
//  Created by Jeferson Alvarenga on 06/05/18.
//  Copyright © 2018 DeepLogical. All rights reserved.
//

import UIKit

class PlaceTableViewCell: UITableViewCell {
    
    static let Identifier = "PlaceTableViewCell"
    
    @IBOutlet private var lblCity: UILabel!
    @IBOutlet private var lblState: UILabel!
    
    func configureWithPlace(place: Place) {
        lblCity.text = "Cidade: \(place.city ?? "")" // TODO Localize string
        lblState.text = "Estado: \(place.state ?? "")" // TODO Localize string
    }
}
