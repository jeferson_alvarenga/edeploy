//
//  SearchResultViewController.swift
//  EDeployChallenge
//
//  Created by Jeferson Alvarenga on 06/05/18.
//  Copyright © 2018 DeepLogical. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import NVActivityIndicatorView

class SearchResultViewController: UIViewController, SearchResultViewModelDelegate, NVActivityIndicatorViewable {

    @IBOutlet private var tableView: UITableView!
    @IBOutlet private var searchBar: UISearchBar!
    
    var places: Variable<[Place]>!
    var fullPlacesList: [Place]!
    var selectedPlace: Place!
    var viewModel: SearchResultViewModel!
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        title = "Resultado" // TODO Localize string
        viewModel = SearchResultViewModel(delegate: self)
        searchBar.searchBarStyle = UISearchBarStyle.prominent
        searchBar.placeholder = "Buscar" // TODO Localize string
        searchBar.sizeToFit()
        setupSearchbar()
        setupCellConfiguration()
        setupCellTapHandling()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addPlaces(items: [Place]) {
        fullPlacesList = items
        places = Variable<[Place]>(items)
    }
    
    private func setupSearchbar () {
        searchBar
            .rx.text
            .orEmpty
            .debounce(0.5, scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .subscribe(onNext: { [unowned self] query in
                self.places.value = self.fullPlacesList
                if !query.isEmpty {
                    self.places.value = self.places.value.filter { ($0.city?.lowercased().contains(query.lowercased()))! }
                }
                self.tableView.reloadData()
            })
            .disposed(by: disposeBag)
    }
    
    private func setupCellConfiguration() {
        places.asObservable()
            .bind(to: tableView
                .rx
                .items(cellIdentifier: PlaceTableViewCell.Identifier,
                       cellType: PlaceTableViewCell.self)) {
                        row, place, cell in
                        cell.configureWithPlace(place: place)
            }
            .disposed(by: disposeBag)
    }
    
    private func setupCellTapHandling() {
        tableView
            .rx
            .modelSelected(Place.self)
            .subscribe(onNext: {
                place in
                self.selectedPlace = place
                self.viewModel.selectedPlace = place
                self.viewModel.loadPlace()
                if let selectedRowIndexPath = self.tableView.indexPathForSelectedRow {
                    self.tableView.deselectRow(at: selectedRowIndexPath, animated: true)
                }
            })
            .disposed(by: disposeBag)
    }
    
    func onPlaceLoaded(value: NSDecimalNumber) {
        stopAnimating()
        self.performSegue(withIdentifier: "seguePlaceDetail", sender: value)
    }
    
    func startAnimating() {
        startAnimating(CGSize(width: 30, height: 30), message: "Buscando...", type: .ballRotateChase) // TODO Localize string
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "seguePlaceDetail" {
            let vc = segue.destination as! PlaceDetailViewController
            vc.place = selectedPlace
            vc.score = sender as? NSDecimalNumber
        }
    }
}
