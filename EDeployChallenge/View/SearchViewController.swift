//
//  SearchViewController.swift
//  EDeployChallenge
//
//  Created by Jeferson Alvarenga on 05/05/18.
//  Copyright © 2018 DeepLogical. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JDropDownAlert

class SearchViewController: UIViewController, SearchViewModelDelegate, NVActivityIndicatorViewable {
    
    @IBOutlet weak var txtState: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var btnSearch: UIButton!
    var viewModel: SearchViewModel!
    var activeIndicator: NVActivityIndicatorView!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        title = "Buscar" // TODO Localize string
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel = SearchViewModel(delegate: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onSearchTouch(_ sender: Any) {
        if validateFields() {
            txtState.resignFirstResponder()
            txtCity.resignFirstResponder()
            viewModel.loadPlaces(city: txtCity.text!, state: txtState.text!)
        }else{
            let alert = JDropDownAlert(position: .top, direction: .normal)
            alert.alertWith("Erro", message: "Por favor preencha todos os campos.")// TODO Localize string
        }
    }
    
    func validateFields() -> Bool {
        guard let city = txtCity.text, !city.isEmpty else {
            return false
        }
        
        guard let state = txtState.text, !state.isEmpty else {
            return false
        }
        
        return true
    }
    
    func startAnimating() {
        startAnimating(CGSize(width: 30, height: 30), message: "Buscando...", type: .ballRotateChase)// TODO Localize string
    }
    
    func onPlacesLoaded() {
        self.stopAnimating()
        self.performSegue(withIdentifier: "seguePlacesResult", sender: nil)
    }
    
    func onPlacesLoadedError(error: String) {
        stopAnimating()
        let alert = JDropDownAlert(position: .top, direction: .normal)
        alert.alertWith("Erro", message: error)// TODO Localize string
    }
    
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "seguePlacesResult" {
            let vc = segue.destination as! SearchResultViewController
            if let places = viewModel.places {
                vc.addPlaces(items: places)
            }else{
                vc.addPlaces(items: [])
            }
        }
     }
}

