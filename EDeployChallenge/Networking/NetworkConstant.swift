//
//  NetworkConstant.swift
//  EDeployChallenge
//
//  Created by Jeferson Alvarenga on 07/05/18.
//  Copyright © 2018 DeepLogical. All rights reserved.
//

import UIKit

class NetworkConstant: NSObject {
    static let placesAPI = "http://wsteste.devedp.com.br/Master/CidadeServico.svc/rest/BuscaTodasCidades"
    static let placeAPI = "http://wsteste.devedp.com.br/Master/CidadeServico.svc/rest/BuscaPontos"
}
