//
//  Networking.swift
//  EDeployChallenge
//
//  Created by Jeferson Alvarenga on 06/05/18.
//  Copyright © 2018 DeepLogical. All rights reserved.
//

import Alamofire
import AlamofireObjectMapper

enum NetworkError: String {
    case ValueError
    case NoInternetConnection = "Sem conexão com a internet." // TODO Localize string
    case Unknown
}

class Networking {
    
    typealias onSuccess = (Any) -> Void
    typealias onFailure = (String) -> Void
    
    func requestMappable<T:ModelBase>(_ url: String, parameters: [String:Any]? = nil, method: String? = "GET", mappable: T.Type, onSuccessMappable: @escaping ([T]) -> Void, onFailure: @escaping onFailure) {
        
        if !checkInternetConnection() {
            onFailure(NetworkError.NoInternetConnection.rawValue)
            return
        }
        
        var httpMethod = HTTPMethod.get
        
        if let parameterMethod = method {
            httpMethod = HTTPMethod(rawValue: parameterMethod)!
        }
        
        Alamofire.request(url, method: httpMethod, parameters: parameters, encoding: JSONEncoding.default).validate().responseArray { (response: DataResponse<[T]>) in
            if response.result.isSuccess {
                if let value = response.result.value {
                    onSuccessMappable(value as [T])
                }else{
                    onFailure(NetworkError.ValueError.rawValue)
                }
            }else{
                onFailure(response.error?.localizedDescription ?? NetworkError.Unknown.rawValue)
            }
        }
    }
    
    func request(_ url: String, parameters: [String:Any]? = nil, method: String? = nil, onSuccess: @escaping onSuccess, onFailure: @escaping onFailure) {
        
        if !checkInternetConnection() {
            onFailure(NetworkError.NoInternetConnection.rawValue)
            return
        }
        
        var httpMethod = HTTPMethod.get
        
        if let parameterMethod = method {
            httpMethod = HTTPMethod(rawValue: parameterMethod)!
        }
        
        Alamofire.request(url, method: httpMethod, parameters: parameters, encoding: JSONEncoding.default).validate().responseJSON { (response) in
            if response.result.isSuccess {
                if let value = response.result.value {
                    onSuccess(value)
                }else{
                    onFailure(NetworkError.ValueError.rawValue)
                }
            }else{
                onFailure(response.error?.localizedDescription ?? NetworkError.Unknown.rawValue)
            }
        }
    }
    
    func checkInternetConnection() -> Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}
