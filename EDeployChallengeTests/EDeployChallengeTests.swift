//
//  EDeployChallengeTests.swift
//  EDeployChallengeTests
//
//  Created by Jeferson Alvarenga on 05/05/18.
//  Copyright © 2018 DeepLogical. All rights reserved.
//

import XCTest
@testable import EDeployChallenge

class EDeployChallengeTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testLoadPlaces() {
        let expectation = XCTestExpectation(description: "Load places API")
        let networking = Networking()
        networking.requestMappable(NetworkConstant.placesAPI, mappable: Place.self, onSuccessMappable: { (response) in
            expectation.fulfill()
        }, onFailure: { (error) in
            expectation.fulfill()
            XCTFail("\(error)")
        })
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testLoadPlaceScore() {
        let expectation = XCTestExpectation(description: "Load places score API")
        let networking = Networking()
        networking.request(NetworkConstant.placeAPI, parameters: ["Nome":"São Paulo", "Estado":"São Paulo"], method: "POST", onSuccess: { (response) in
            expectation.fulfill()
        }, onFailure: { (error) in
            expectation.fulfill()
            XCTFail("\(error)")
        })
        wait(for: [expectation], timeout: 10.0)
    }
}
